package sample;

import javafx.animation.KeyFrame;
import javafx.animation.Timeline;
import javafx.concurrent.Task;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.fxml.Initializable;
import javafx.scene.control.Button;
import javafx.scene.control.ProgressBar;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.util.Duration;

import java.io.File;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;
import java.util.ResourceBundle;
import java.util.stream.IntStream;


public class Controller implements Initializable {
    public ProgressBar pbWorking;
    public Button btnWork;
    public ImageView iv;
    List<String> imagepath = new ArrayList<>();

    
    public void onWorking(ActionEvent actionEvent) {
        Timeline timeline = new Timeline();
        timeline.setCycleCount(Timeline.INDEFINITE);
        //timeline.setAutoReverse(false);


        Task task = new Task<Void>() {
            @Override
            protected Void call() throws Exception {
                final int max = 80;
                for (int i = 1; i < max; i++) {

                    updateProgress(i, max);
                    Thread.sleep(80);
                }
                return null;
            }

            @Override
            protected void scheduled() {
                super.scheduled();
                timeline.play();
                pbWorking.setVisible(true);
                iv.setVisible(true);
            }

            @Override
            protected void succeeded() {
                super.succeeded();
                timeline.stop();
                pbWorking.setVisible(false);
                iv.setVisible(false);
            }
        };

        pbWorking.progressProperty().bind(task.progressProperty());

        new Thread(task).start();
        pbWorking.setVisible(true);

        for (int i=0;i<5;i++){
            int j=i;
            timeline.getKeyFrames().add(
                    new KeyFrame(Duration.seconds(i + 1), new EventHandler<ActionEvent>() {
                        @Override
                        public void handle(ActionEvent event) {
                            iv.setImage(new Image(getClass().getResource(imagepath.get(j)).toString()));
                        }
                    })
            );
        }


    }


    public void moveFile(String fromPath,String toPath){
        File fromFolder = new File(fromPath);
        File [] fromFiles=fromFolder.listFiles();
        if (fromFiles==null) {
            return;
        }
        File toFolder=new File(toPath);
        if (!toFolder.exists()) {
            toFolder.mkdirs();
        }
        for (int i = 0; i < fromFiles.length; i++) {
            File file = fromFiles[i];
            if (file.isDirectory()) {
                moveFile(file.getPath(),toPath+"\\"+file.getName());
                //亦可删除
                //file.delete();
            }
            if (file.isFile()) {
                File toFile=new File(toFolder+"\\"+file.getName());
                if (toFile.exists()) {
                    //亦可删除
                    //toFile.delete();
                }
                //移动文件
                file.renameTo(toFile);
            }

        }
    }

    @Override
    public void initialize(URL location, ResourceBundle resources) {
        pbWorking.setProgress(ProgressBar.INDETERMINATE_PROGRESS);
        for (int i = 0; i < 5; i++) {
            imagepath.add("/resources/".concat(String.valueOf(i)).concat(".png"));
        }
        iv.setImage(new Image(getClass().getResource("/resources/4.png").toString()));
    }
}
